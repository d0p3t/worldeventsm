﻿using System;
using System.Collections.Generic;

namespace WorldEventsM.Shared.Model
{
    public class WorldEventPlayer
    {
        public string LicenseIdentifier { get; set; }
        public List<PlayerScore> PlayerScores { get; set; } = new List<PlayerScore>();
        public int Level { get; set; } = 1;
        public int TotalXp { get; set; } = 0;
        public bool IsOnline { get; set; } = true;
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
        public DateTime UpdatedAt { get; set; } = DateTime.UtcNow;
    }
}
