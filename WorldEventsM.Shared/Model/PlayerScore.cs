﻿namespace WorldEventsM.Shared.Model
{
    public class PlayerScore
    {
        public int EventId { get; set; }
        public float EventXpMultiplier { get; set; } = 1.0f;
        public float CurrentAttempt { get; set; }
        public float BestAttempt { get; set; }
    }
}
