﻿namespace WorldEventsM.Shared.Model
{
    public class PlayerSettings
    {
        public bool PvPEnabled { get; set; } = true;
        public float VoiceDistance { get; set; } = 20f;
    }
}
