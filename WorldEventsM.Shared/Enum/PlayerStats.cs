﻿namespace WorldEventsM.Shared.Enum
{
    public enum PlayerStats : int
    {
        NumberNearMisses = -1058879197,
        //NumberNearMissesNoCrash = -791767133,
        FastestSpeedInCar = -2133168010,
        MostPistolHeadshots = -252472232,
        LONGEST_2WHEEL_DIST = 1061163284,
        LONGEST_2WHEEL_TIME = 1575087458,
        LongestSurvivedFreefall = 742230262,
        HighestSkittles = -1411284426,
        FarthestJumpDistance = 908833912,
        HighestJumpDistance = 1831206202,
        FlyUnderBridges = -1169860382
    }

    public enum PlayerStatType
    {
        Int,
        Float,
        String
    }
}
