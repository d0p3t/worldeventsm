﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorldEventsM.Shared.Model;

namespace WorldEventsM.Shared.Helpers
{
    public static class Constants
    {
        public static readonly string ExtraPlayer1 = "Player 1";
        public static readonly string ExtraPlayer2 = "Player 2";
        public static WorldEventPlayer WorldEventPlayer = new WorldEventPlayer();
    }
}
