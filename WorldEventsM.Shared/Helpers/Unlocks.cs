﻿using System.Collections.Generic;

namespace WorldEventsM.Shared.Helpers
{
    public static class Unlocks
    {
        public static Dictionary<string, int> Vehicles = new Dictionary<string, int>
        {
            { "bfinjection", 1 },
            { "airtug", 1 },
            { "blista", 1 },
            { "panto", 1 },
            { "faggio", 1 },
            { "microlight", 1 },
            { "rhapsody", 2 },
            { "scorcher", 2 },
            { "havok", 2 },
            { "vader", 2 },
            { "exemplar", 3 },
            { "akuma", 3 },
            { "buccaneer2", 3 },
            { "blazer", 3 },
            { "duster", 4 },
            { "hydra", 5 },
            { "sanchez2", 5 },
            { "bestiagts", 5 },
            { "swift", 5 },
            { "jester2", 5 },
            { "baller5", 6 },
            { "stretch", 6 },
            { "asea", 6 },
            { "infernus2", 7 },
            { "dubsta2", 7 },
            { "hakuchou", 8 },
            { "buzzard2", 8 },
            { "velum", 9 },
            { "penetrator", 9 },
            { "tractor2", 9 },
            { "stinger", 10 },
            { "ruston", 10 },
            { "rallytruck", 10 },
            { "shamal", 10 },
            { "trophytruck", 10 },
            { "molotok", 11 },
            { "prototipo", 12 },
            { "speedo2", 13 },
            { "cargobob2", 14 },
            { "monster", 14 },
            { "dune4", 14 },
            { "shotaro", 15 },
        };
    }
}
