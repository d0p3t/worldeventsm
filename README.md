# WorldEventsM
Gametype Contest Submission by d0p3t

In short, WorldEventsM is a gamemode that makes use of MP Player Stats to recreate GTA: Online freemode challenges.

---

Server Bazaar Topic: https://forum.fivem.net/t/worldeventsm-alpha-freemode-challenges-xp-system-number-of-near-misses-flying-under-bridges-most-pistol-headshots-and-more-freeroam-gametype-contest/208575

IP: d0p3t.nl:30130 (or 64.190.90.120:30130)

---

# Instructions
1. Extract the contents of the zip file found in `Resource` folder to your servers resources folder
2. Add `worldevents`, `worldevents-map` and `worldevents-ls` to your `server.cfg`
3. (Optional) Add your sentry key to the server.cfg as a convar `set sentry_key "http://yourfullkeytoken"`

More info: https://forum.fivem.net/t/worldeventsm-alpha-freemode-challenges-xp-system-number-of-near-misses-flying-under-bridges-most-pistol-headshots-and-more-freeroam-gametype-contest/208575