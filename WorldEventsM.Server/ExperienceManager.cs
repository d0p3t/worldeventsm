﻿using System;
using System.Linq;
using CitizenFX.Core;
using WorldEventsM.Shared.Helpers;

namespace WorldEventsM.Server
{
    public class ExperienceManager : BaseScript
    {
        public ExperienceManager()
        {
            Server.GetInstance().RegisterEventHandler("worldEventsManage.Internal:AddExperience", new Action<string, int>(OnAddExperience));
        }

        private void OnAddExperience(string identifier, int experiencePoints)
        {
            try
            {
                var leveledUp = false;
                var currentLevel = PlayerCache.GetCurrentLevel(identifier);
                var currentRankLimit = Experience.RankRequirement.Where(x => x.Key == currentLevel).First().Value;
                var nextRankLimit = Experience.NextLevelExperiencePoints(currentLevel);
                var currentXp = PlayerCache.GetCurrentExperiencePoints(identifier);

                PlayerCache.AddExperience(identifier, experiencePoints);

                var updatedLevel = PlayerCache.GetCurrentLevel(identifier);
                var updatedXp = PlayerCache.GetCurrentExperiencePoints(identifier);
                var updatedCurrentRankLimit = currentRankLimit;
                var updatedNextRankLimit = nextRankLimit;

                if (currentLevel != updatedLevel)
                {
                    updatedCurrentRankLimit = Experience.RankRequirement.Where(x => x.Key == updatedLevel).First().Value;
                    updatedNextRankLimit = Experience.NextLevelExperiencePoints(updatedLevel);

                    // assume you can only level up once
                    leveledUp = true;
                }

                var pl = new PlayerList();
                foreach (var player in pl)
                {
                    if (player.Identifiers["license"] == identifier)
                    {
                        player.TriggerEvent("worldEventsManage.Client.UpdateExperience", currentRankLimit, nextRankLimit, updatedCurrentRankLimit, updatedNextRankLimit, currentXp, updatedXp, currentLevel, updatedLevel, leveledUp);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }
    }
}
