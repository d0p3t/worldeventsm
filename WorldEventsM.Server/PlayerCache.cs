﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using CitizenFX.Core;
using static CitizenFX.Core.Native.API;
using Newtonsoft.Json;
using WorldEventsM.Shared.Model;
using WorldEventsM.Shared.Helpers;

namespace WorldEventsM.Server
{
    public static class PlayerCache
    {
        private static List<WorldEventPlayer> PlayersList = new List<WorldEventPlayer>();

        public static void PlayerOffline(string identifier)
        {
            try
            {
                var player = PlayersList.Where(x => x.LicenseIdentifier == identifier).FirstOrDefault();
                if (player != null)
                {
                    player.IsOnline = false;
                }
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }

        public static bool AddPlayer(string identifier)
        {
            try
            {
                if (PlayersList.Where(x => x.LicenseIdentifier == identifier).Any())
                {
                    var player = PlayersList.Where(x => x.LicenseIdentifier == identifier).FirstOrDefault();
                    if (player != null)
                    {
                        player.IsOnline = true;
                    }
                    return false;
                }

                var highscores = new List<PlayerScore>();

                foreach (var worldEvent in WorldEventsManager.WorldEvents)
                {
                    highscores.Add(new PlayerScore { EventId = worldEvent.Id, BestAttempt = 0, CurrentAttempt = 0, EventXpMultiplier = worldEvent.EventXpMultiplier });
                }

                var newPlayer = new WorldEventPlayer { LicenseIdentifier = identifier, PlayerScores = highscores };

                PlayersList.Add(newPlayer);
                return true;
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }

            return false;
        }

        public static void UpdateCurrentAttempt(string identifier, int eventId, float currentAttempt)
        {
            try
            {
                var player = PlayersList.Where(x => x.LicenseIdentifier == identifier).FirstOrDefault();
                if (player != null)
                {
                    var data = player.PlayerScores.Where(x => x.EventId == eventId).FirstOrDefault();
                    if (data != null)
                    {
                        data.CurrentAttempt = currentAttempt;
                        if (currentAttempt > data.BestAttempt)
                        {
                            data.BestAttempt = currentAttempt;
                        }
                        player.UpdatedAt = DateTime.UtcNow;
                    }
                    else
                    {
                        Logger.Warning($"Data for Event {eventId} does not exist for Player {identifier}");
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }

        public static void ResetAllCurrentAttempts(int eventId)
        {
            try
            {
                foreach (var player in PlayersList)
                {
                    player.PlayerScores.First(x => x.EventId == eventId).CurrentAttempt = 0;
                    player.UpdatedAt = DateTime.UtcNow;
                }
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }

        public static void UpdateBestAttempt(string identifier, int eventId, int bestAttempt)
        {
            try
            {
                var player = PlayersList.Where(x => x.LicenseIdentifier == identifier).FirstOrDefault();
                if (player != null)
                {
                    var score = player.PlayerScores.Where(x => x.EventId == eventId).FirstOrDefault();
                    if (score.BestAttempt < bestAttempt)
                    {
                        score.BestAttempt = bestAttempt;
                        player.UpdatedAt = DateTime.UtcNow;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }

        public static void SendFinalTop3Players(int eventId, float eventMultiplier)
        {
            try
            {
                var pl = new PlayerList();

                if (pl.Count() == 0 || !PlayersList.Where(x => x.IsOnline == true).Any()) { return; }

                var tempDictionary = new Dictionary<string, float>();

                foreach (var player in PlayersList)
                {
                    var score = player.PlayerScores.Where(x => x.EventId == eventId).FirstOrDefault();
                    if (score != null)
                    {
                        foreach (var p in pl)
                        {
                            if (p.Identifiers["license"] == player.LicenseIdentifier)
                            {
                                var xpGain = (int)Math.Min(score.CurrentAttempt * eventMultiplier, Experience.RankRequirement[player.Level + 1] - Experience.RankRequirement[player.Level]);

                                if (xpGain != 0)
                                    BaseScript.TriggerEvent("worldEventsManage.Internal:AddExperience", player.LicenseIdentifier, xpGain);

                                tempDictionary.Add(p.Name, score.CurrentAttempt);
                            }
                        }
                    }
                }

                if(tempDictionary.Count == 0)
                {
                    tempDictionary.Add("-1", 0);
                    tempDictionary.Add("-2", 0);
                    tempDictionary.Add("-3", 0);
                }
                else if (tempDictionary.Count == 1)
                {
                    tempDictionary.Add("-1", 0);
                    tempDictionary.Add("-2", 0);
                }
                else if (tempDictionary.Count == 2)
                {
                    tempDictionary.Add("-1", 0);
                }

                tempDictionary.OrderByDescending(x => x.Value);

                var newerDict = tempDictionary.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);

                BaseScript.TriggerClientEvent("worldEventsManage.Client:FinalTop3", eventId, JsonConvert.SerializeObject(newerDict));
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }

        public static void SendTop3Players(int eventId)
        {
            try
            {
                var pl = new PlayerList();
                if (pl.Count() == 0 || PlayersList.Count == 0) { return; } // should never happen but :shrug:

                var tempDictionary = new Dictionary<string, float>();

                foreach (var player in PlayersList)
                {
                    if (player.IsOnline)
                    {
                        var score = player.PlayerScores.Where(x => x.EventId == eventId).FirstOrDefault();
                        if (score != null)
                        {
                            foreach (var playa in pl)
                            {
                                if (playa.Identifiers["license"] == player.LicenseIdentifier)
                                {
                                    tempDictionary.Add(playa.Name, score.CurrentAttempt);
                                }
                            }
                        }
                    }
                }

                if (tempDictionary.Count == 1)
                {
                    tempDictionary.Add(Constants.ExtraPlayer1, 0);
                    tempDictionary.Add(Constants.ExtraPlayer2, 0);
                }
                else if (tempDictionary.Count == 2)
                {
                    tempDictionary.Add(Constants.ExtraPlayer1, 0);
                }

                var newDict = tempDictionary.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);

                if (newDict.Count < 3) { return; }

                BaseScript.TriggerClientEvent("worldEventsManage.Client:GetTop3", JsonConvert.SerializeObject(newDict));
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }

        public static void SendEventData(int eventId)
        {
            try
            {
                var pl = new PlayerList();

                foreach (var p in pl)
                {
                    var player = PlayersList.Where(x => x.LicenseIdentifier == p.Identifiers["license"]).FirstOrDefault();
                    if (player != null)
                    {
                        var eventData = player.PlayerScores.Where(x => x.EventId == eventId).FirstOrDefault();
                        p.TriggerEvent("worldeventsManager.Client:GetEventData", eventData.EventId, eventData.CurrentAttempt, eventData.BestAttempt);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }

        public async static Task SavePlayersData()
        {
            try
            {
                var json = JsonConvert.SerializeObject(PlayersList, Formatting.Indented);

                if(string.IsNullOrEmpty(json)) { return; }

                using (StreamWriter file = File.CreateText($@"resources/{GetCurrentResourceName()}/worldplayers.json"))
                {
                    await file.WriteAsync(json);
                }

                Logger.Info($"PlayerCache Saved to worldplayers.json");
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }

            await Task.FromResult(0);
        }

        public async static Task LoadPlayersData()
        {
            try
            {
                if (!File.Exists($@"resources/{GetCurrentResourceName()}/worldplayers.json"))
                {
                    File.Create($@"resources/{GetCurrentResourceName()}/worldplayers.json");
                }

                using (StreamReader file = File.OpenText($@"resources/{GetCurrentResourceName()}/worldplayers.json"))
                {
                    var data = file.ReadToEnd();
                    var json = JsonConvert.DeserializeObject<List<WorldEventPlayer>>(data);
                    foreach (var d in json)
                    {
                        foreach (var s in d.PlayerScores)
                        {
                            s.CurrentAttempt = 0;
                        }
                    }

                    PlayersList.AddRange(json);
                    Logger.Info($"Loaded {PlayersList.Count} Player Profiles.");
                }
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }

            await Task.FromResult(0);
        }

        public static int GetCurrentLevel(string identifier)
        {
            try
            {
                var player = PlayersList.Where(x => x.LicenseIdentifier == identifier).FirstOrDefault();

                if (player != null)
                {
                    return player.Level;
                }
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }

            return -1;
        }

        public static int GetCurrentExperiencePoints(string identifier)
        {
            try
            {
                var player = PlayersList.Where(x => x.LicenseIdentifier == identifier).FirstOrDefault();

                if (player != null)
                {
                    return player.TotalXp;
                }
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }

            return 0;
        }

        public static void AddExperience(string identifier, int experiencePoints)
        {
            try
            {
                var player = PlayersList.Where(x => x.LicenseIdentifier == identifier && x.IsOnline).FirstOrDefault();
                if (player != null)
                {
                    var nextLevelTotalXp = Experience.NextLevelExperiencePoints(player.Level);

                    if (player.TotalXp + experiencePoints >= nextLevelTotalXp)
                    {
                        int remainder = player.TotalXp + experiencePoints - nextLevelTotalXp;

                        player.Level++;

                        if (remainder > 0)
                        {
                            AddExperience(identifier, remainder);
                        }
                    }
                    else
                    {
                        player.TotalXp += experiencePoints;
                    }

                    player.UpdatedAt = DateTime.UtcNow;
                }
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }
    }
}
