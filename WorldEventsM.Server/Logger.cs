﻿using System;
using SharpRaven;
using SharpRaven.Data;
using WorldEventsM.Shared.Model;

namespace WorldEventsM.Server
{
    public static class Logger
    {
        private static RavenClient ravenClient = new RavenClient(CitizenFX.Core.Native.API.GetConvar("sentry_key", "http://9d1caf7c72df4f588f9331ca019c11fc@sentry.io/826750"));

        private static readonly LogLevel enableDebug = LogLevel.Info;

        public static void Debug(string message)
        {
            if (enableDebug <= LogLevel.Debug) { CitizenFX.Core.Debug.WriteLine($"[{DateTime.UtcNow}][DEBUG] {message}"); }
        }

        public static void Info(string message)
        {
            if (enableDebug <= LogLevel.Info) { CitizenFX.Core.Debug.WriteLine($"[{DateTime.UtcNow}][INFO] {message}"); }
        }

        public async static void Warning(Exception e, string message)
        {
            if (enableDebug <= LogLevel.Warning)
            {
                CitizenFX.Core.Debug.WriteLine($"[{DateTime.UtcNow}][WARNING] {e} | {message}");
                await ravenClient.CaptureAsync(new SentryEvent(e));
            }
        }

        public async static void Warning(string message)
        {
            if (enableDebug <= LogLevel.Warning)
            {
                CitizenFX.Core.Debug.WriteLine($"[{DateTime.UtcNow}][WARNING] {message}");
                await ravenClient.CaptureAsync(new SentryEvent(message));
            }
        }

        public async static void Exception(Exception e, string message = "")
        {
            if (enableDebug <= LogLevel.Error)
            {
                CitizenFX.Core.Debug.WriteLine($"[{DateTime.UtcNow}][EXCEPTION] {e} {message}");
                await ravenClient.CaptureAsync(new SentryEvent(e));
            }
        }
    }

    public enum LogLevel
    {
        None,
        Debug,
        Info,
        Warning,
        Error
    }
}
