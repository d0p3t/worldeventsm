﻿using System;
using System.Collections.Generic;
using CitizenFX.Core;

namespace WorldEventsM.Server
{
    public class VehicleManager : BaseScript
    {
        public static List<int> SpawnedEventVehicles = new List<int>();

        public VehicleManager()
        {
            Server.GetInstance().RegisterEventHandler("worldEventsManage.Server:SpawnedEventVehicles", new Action<Player,List<dynamic>>(OnSpawnedEventVehicles));
        }

        private void OnSpawnedEventVehicles([FromSource]Player player, List<dynamic> dynamicVehicles)
        {
            try
            {
                SpawnedEventVehicles.Clear();
                foreach (var v in dynamicVehicles)
                {
                    SpawnedEventVehicles.Add((int)v);
                }

                BaseScript.TriggerClientEvent("worldEventsManage.Client:SetVehicleBlips", SpawnedEventVehicles);
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }
    }
}
