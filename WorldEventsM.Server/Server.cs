﻿using System;
using System.Threading.Tasks;
using CitizenFX.Core;

namespace WorldEventsM.Server
{
    public class Server : BaseScript
    {
        private static Server _instance;

        public static Server GetInstance()
        {
            return _instance;
        }

        public Server()
        {
            _instance = this;
        }

        public void RegisterTickHandler(Func<Task> action)
        {
            try
            {
                Tick += action;
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }

        public void DeregisterTickHandler(Func<Task> action)
        {
            try
            {
                Tick -= action;
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }

        public void RegisterEventHandler(string name, Delegate action)
        {
            try
            {
                EventHandlers.Add(name, action);
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }

        public void DeregisterEventHandler(string name)
        {
            try
            {
                EventHandlers.Remove(name);
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }
    }
}
