﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using CitizenFX.Core;
using static CitizenFX.Core.Native.API;
using WorldEventsM.Shared.Model;
using Newtonsoft.Json;

namespace WorldEventsM.Server
{
    public class WorldEventsManager : BaseScript
    {
        public static readonly List<WorldEvent> WorldEvents = new List<WorldEvent>
        {
            new WorldEvent{ Id = 1, Name = "Number Of Near Misses", CountdownTime = TimeSpan.FromSeconds(60), EventTime = TimeSpan.FromSeconds(300), EventXpMultiplier = 13.5f },
            new WorldEvent{ Id = 2, Name = "Flying Under Bridges", CountdownTime = TimeSpan.FromSeconds(90), EventTime = TimeSpan.FromSeconds(300), EventXpMultiplier = 35.25f },
            new WorldEvent{ Id = 3, Name = "Fastest Speed", CountdownTime = TimeSpan.FromSeconds(60), EventTime = TimeSpan.FromSeconds(270), EventXpMultiplier = 1.5f},
            new WorldEvent{ Id = 4, Name = "Most Pistol Headshots", CountdownTime = TimeSpan.FromSeconds(60), EventTime = TimeSpan.FromSeconds(300), EventXpMultiplier = 20.5f},
            new WorldEvent{ Id = 5, Name = "Longest Survived Freefall", CountdownTime = TimeSpan.FromSeconds(60), EventTime = TimeSpan.FromSeconds(240), EventXpMultiplier = 100.5f},
            new WorldEvent{ Id = 6, Name = "Highest Skittles", CountdownTime = TimeSpan.FromSeconds(60), EventTime = TimeSpan.FromSeconds(300), EventXpMultiplier = 13.5f},
            new WorldEvent{ Id = 7, Name = "Longest Jump", CountdownTime = TimeSpan.FromSeconds(60), EventTime = TimeSpan.FromSeconds(270), EventXpMultiplier = 20.5f},
            new WorldEvent{ Id = 8, Name = "Highest Jump", CountdownTime = TimeSpan.FromSeconds(60), EventTime = TimeSpan.FromSeconds(270), EventXpMultiplier = 25.5f},
        };

        private static readonly int SaveInterval = 5;

        private static WorldEvent CurrentEvent;
        private static WorldEvent NextEvent;

        private static TimeSpan TimeUntilNextEvent = TimeSpan.FromSeconds(5);
        private static Random rnd = new Random();

        private static bool IsAnyEventActive = false;

        public WorldEventsManager()
        {
            Server.GetInstance().RegisterEventHandler("worldEventsManage.Server:AddParticipant", new Action<Player>(OnAddParticipant));
            Server.GetInstance().RegisterEventHandler("worldEventsManage.Server:EventEnded", new Action<Player, int, int, int>(OnEventEnded));
            Server.GetInstance().RegisterEventHandler("worldEventsManage.Server:UpdateCurrentEvent", new Action<Player, int, float>(OnUpdateCurrentEvent));
            Server.GetInstance().RegisterEventHandler("worldEventsManage.Server:GetStatus", new Action<Player>(OnGetStatus));
            Server.GetInstance().RegisterEventHandler("onServerResourceStart", new Action<string>(OnServerResourceStart));
            Server.GetInstance().RegisterEventHandler("playerDropped", new Action<Player, string>(OnPlayerDropped));

            Server.GetInstance().RegisterTickHandler(OnSaveDataTick);
            Server.GetInstance().RegisterTickHandler(OnPeriodicTick);
            Server.GetInstance().RegisterTickHandler(OnEventTick);
        }

        private void OnPlayerDropped([FromSource] Player player, string reason)
        {
            PlayerCache.PlayerOffline(player.Identifiers["license"]);
            Logger.Info($"Player {player.Identifiers["license"]} disconnected ({reason})");
        }

        private void OnUpdateCurrentEvent([FromSource]Player player, int eventId, float currentAttempt)
        {
            if(CurrentEvent == null) { return; }
            if(CurrentEvent.Id != eventId) { return; }

            PlayerCache.UpdateCurrentAttempt(player.Identifiers["license"], eventId, currentAttempt);
        }

        private async Task OnPeriodicTick()
        {
            try
            {
                await BaseScript.Delay(1000);
                if (CurrentEvent.CountdownTime != TimeSpan.Zero)
                {
                    BaseScript.TriggerClientEvent("worldEventsManage.Client:PeriodicSync", (int)CurrentEvent.CountdownTime.TotalSeconds, false);
                    return;
                }

                BaseScript.TriggerClientEvent("worldEventsManage.Client:PeriodicSync", (int)CurrentEvent.EventTime.TotalSeconds, true);
            }
            catch (Exception e)
            {
                Logger.Exception(e, $"{e.Source}.{e.TargetSite}()");
            }

            await Task.FromResult(0);
        }

        private async Task OnSaveDataTick()
        {
            try
            {
                await BaseScript.Delay(60000 * SaveInterval);
                await PlayerCache.SavePlayersData();
            }
            catch (Exception e)
            {
                Logger.Exception(e, $"{e.Source}.{e.TargetSite}()");
            }

            await Task.FromResult(0);
        }

        private async Task OnEventTick()
        {
            try
            {
                if (!IsAnyEventActive)
                {
                    await BaseScript.Delay(1000);

                    TimeUntilNextEvent = TimeUntilNextEvent.Subtract(TimeSpan.FromSeconds(1));

                    if (TimeUntilNextEvent == TimeSpan.Zero)
                    {
                        NextEvent.IsActive = true;
                        NextEvent.IsStarted = false;
                        IsAnyEventActive = NextEvent.IsActive;
                        CurrentEvent = NextEvent;
                        PlayerCache.SendEventData(CurrentEvent.Id);
                        ChooseNextEvent();

                        Logger.Info($"Current Event [{CurrentEvent.Name}] | Next Event [{NextEvent.Name}]");
                        BaseScript.TriggerClientEvent("worldEventsManage.Client:EventActivate", CurrentEvent.Id, NextEvent.Id);
                    }
                }
                else
                {
                    if (!CurrentEvent.IsStarted)
                    {
                        await BaseScript.Delay(1000);
                        CurrentEvent.CountdownTime = CurrentEvent.CountdownTime.Subtract(TimeSpan.FromSeconds(1));
                        if (CurrentEvent.CountdownTime == TimeSpan.Zero)
                        {
                            CurrentEvent.IsStarted = true;
                            BaseScript.TriggerClientEvent("worldEventsManage.Client:EventStart", CurrentEvent.Id);
                        }
                    }
                    else
                    {
                        await BaseScript.Delay(1000);
                        CurrentEvent.EventTime = CurrentEvent.EventTime.Subtract(TimeSpan.FromSeconds(1));

                        if (CurrentEvent.EventTime == TimeSpan.Zero)
                        {
                            IsAnyEventActive = false;
                            await BaseScript.Delay(1500); // Delay to let everyone send in their results
                            PlayerCache.SendFinalTop3Players(CurrentEvent.Id, CurrentEvent.EventXpMultiplier);

                            var cE = JsonConvert.SerializeObject(WorldEvents.Where(x => x.Id == CurrentEvent.Id).FirstOrDefault());
                            CurrentEvent = JsonConvert.DeserializeObject<WorldEvent>(cE); // reset this element. it's actually the next event
                            CurrentEvent.IsStarted = false;
                            CurrentEvent.IsActive = false;
                            TimeUntilNextEvent = TimeSpan.FromSeconds(5);
                            await BaseScript.Delay(3500); // Wait until we start the next event (total 5 seconds)

                            BaseScript.TriggerClientEvent("worldEventsManage.Client:DestroyEventVehicles");
                            BaseScript.TriggerClientEvent("worldEventsManage.Client:NextEventIn", (int) TimeUntilNextEvent.TotalSeconds);
                        }
                        else
                        {
                            PlayerCache.SendTop3Players(CurrentEvent.Id);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Exception(e, $"{e.Source}.{e.TargetSite}()");
            }

            await Task.FromResult(0);
        }

        private async void OnServerResourceStart(string resourceName)
        {
            if(GetCurrentResourceName() != resourceName) { return; }

            await PlayerCache.LoadPlayersData();
            ChooseNextEvent();
        }

        private void OnEventEnded([FromSource]Player player, int eventId, int currentAttempt, int bestAttempt)
        {
            try
            {
                if (eventId != CurrentEvent.Id) { return; }

                var identifier = player.Identifiers["license"];
                PlayerCache.UpdateCurrentAttempt(identifier, eventId, currentAttempt);
                PlayerCache.UpdateBestAttempt(identifier, eventId, bestAttempt);
            }
            catch (Exception e)
            {
                Logger.Exception(e, $"{e.Source}.{e.TargetSite}()");
            }
        }

        private void OnGetStatus([FromSource] Player player)
        {
            try
            {
                Logger.Debug($"{player.Name} want status");
                var joinWaitTime = 0;
                var isStarted = CurrentEvent.IsStarted;
                if (!CurrentEvent.IsActive)
                {
                    player.TriggerEvent("worldEventsManage.Client:Status", CurrentEvent.Id, NextEvent.Id, (int)TimeUntilNextEvent.TotalSeconds, joinWaitTime, isStarted);

                    return;
                }

                if (CurrentEvent.IsStarted)
                {
                    joinWaitTime = (int)CurrentEvent.EventTime.TotalSeconds;
                }
                else
                {
                    joinWaitTime = (int)CurrentEvent.CountdownTime.TotalSeconds;
                }

                Logger.Debug($"{TimeUntilNextEvent.TotalSeconds} sec");
                player.TriggerEvent("worldEventsManage.Client:Status", CurrentEvent.Id, NextEvent.Id, (int)TimeUntilNextEvent.TotalSeconds, joinWaitTime, isStarted);
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }

        private void OnAddParticipant([FromSource] Player player)
        {
            try
            {
                var identifier = player.Identifiers["license"];

                var success = PlayerCache.AddPlayer(identifier);
                var xp = PlayerCache.GetCurrentExperiencePoints(identifier);
                var level = PlayerCache.GetCurrentLevel(identifier);

                player.TriggerEvent("worldeventsManage.Client:GetLevelXp", level, xp);
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }

        private void ChooseNextEvent()
        {
            try
            {
                if (CurrentEvent == null)
                {
                    var cE = JsonConvert.SerializeObject(WorldEvents.OrderBy(x => rnd.NextDouble()).First());
                    CurrentEvent = JsonConvert.DeserializeObject<WorldEvent>(cE);
                }

                var nE = JsonConvert.SerializeObject(WorldEvents.Where(x => x.Id != CurrentEvent.Id).OrderBy(x => rnd.NextDouble()).First());
                NextEvent = JsonConvert.DeserializeObject<WorldEvent>(nE);
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }
    }
}
