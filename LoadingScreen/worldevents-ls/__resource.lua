
resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'

files {
    'index.html',
    'css/style.css',
	'css/icons.css',
	'fonts/worldevents.woff',
	'assets/cursor.png',
	'assets/music.mp3',
    'img/worldevents-logo.png',
    'img/background.jpg',
    'js/controls.js',
	'js/main.js'
}

loadscreen 'index.html'