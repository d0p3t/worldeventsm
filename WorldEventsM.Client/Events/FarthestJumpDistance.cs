﻿using System;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.UI;
using static CitizenFX.Core.Native.API;
using WorldEventsM.Shared.Enum;

namespace WorldEventsM.Client.Events
{
    public class FarthestJumpDistance : IWorldEvent
    {
        public FarthestJumpDistance(int id, string name, double countdownTime, double seconds) : base(id, name, countdownTime, seconds, false, "AMCH_BIG_0", PlayerStats.FarthestJumpDistance, "m", PlayerStatType.Float)
        {
            Client.GetInstance().RegisterTickHandler(OnTick);
        }

        public override void OnEventActivated()
        {
            FirstStartedTick = true;
            Game.PlayerPed.Weapons.RemoveAll();
            base.OnEventActivated();
        }

        public override void ResetEvent()
        {
            Game.Player.WantedLevel = 0;

            base.ResetEvent();
        }
        private async Task OnTick()
        {
            try
            {
                if (!IsActive) { return; }

                if (!IsStarted)
                {
                    Screen.ShowSubtitle($"Find a land vehicle and Prepare for the {Name} Challenge.", 50);
                }
                else
                {
                    Screen.ShowSubtitle(GetLabelText("AMCH_BIG_0"), 50);
                }

            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }

            await Task.FromResult(0);
        }
    }
}

