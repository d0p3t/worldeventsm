﻿using System;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.UI;
using WorldEventsM.Client.Helpers;
using WorldEventsM.Shared.Enum;

namespace WorldEventsM.Client.Events
{
    class MostPistolHeadshots : IWorldEvent
    {
        public MostPistolHeadshots(int id, string name, double countdownTime, double seconds) : base(id, name, countdownTime, seconds, true, "Perform the Most Headshots with your equipped Pistol", PlayerStats.MostPistolHeadshots)
        {
            Client.GetInstance().RegisterTickHandler(OnTick);
        }
        public override void OnEventActivated()
        {
            FirstStartedTick = true;
            Game.PlayerPed.Weapons.RemoveAll();
            Game.PlayerPed.Weapons.Give(WeaponHash.Pistol, 255, true, false);
            Hud.AdvancedNotification($"You've been given a pistol for the {Name} Challenge", "Weapon Equipped", "", "CHAR_AMMUNATION", "CHAR_AMMUNATION", HudColor.HUD_COLOUR_REDDARK, default(System.Drawing.Color),false, NotificationType.Bubble);
            base.OnEventActivated();
        }

        public override void ResetEvent()
        {
            Game.Player.WantedLevel = 0;

            base.ResetEvent();
        }
        private async Task OnTick()
        {
            try
            {
                if (!IsActive) { return; }

                if (!IsStarted)
                {
                    Screen.ShowSubtitle($"Prepare for the {Name} Challenge using your Pistol.", 50);
                }
                else
                {
                    Screen.ShowSubtitle("Achieve the Most Headshots with a Pistol.", 50);
                }

            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }

            await Task.FromResult(0);
        }
    }
}

