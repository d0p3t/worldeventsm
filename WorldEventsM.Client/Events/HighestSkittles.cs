﻿using System;
using System.Threading.Tasks;
using CitizenFX.Core.UI;
using static CitizenFX.Core.Native.API;
using WorldEventsM.Shared.Enum;

namespace WorldEventsM.Client.Events
{
    public class HighestSkittles : IWorldEvent
    {
        public HighestSkittles(int id, string name, double countdownTime, double seconds) : base(id, name, countdownTime, seconds, true, "Achieve as many vehicular kills as possible", PlayerStats.HighestSkittles)
        {
            Client.GetInstance().RegisterTickHandler(OnTick);
            Client.GetInstance().RegisterTickHandler(OnStartedEventTick);
        }

        public override void OnEventActivated()
        {
            FirstStartedTick = true;
            base.OnEventActivated();
        }
        private async Task OnTick()
        {
            try
            {
                if (!IsActive) { return; }

                if (!IsStarted)
                {
                    Screen.ShowSubtitle($"Acquire a vehicle and prepare for the {Name} Challenge.", 50);
                }
                else
                {
                    Screen.ShowSubtitle("Achieve as many vehicular kills as possible.", 50);
                }

            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }

            await Task.FromResult(0);
        }

        private async Task OnStartedEventTick()
        {
            try
            {
                if (!IsStarted) { return; }

                if (FirstStartedTick)
                {
                    FirstStartedTick = false;
                    var hash = unchecked((uint)PlayerStats.HighestSkittles);
                    StatSetFloat(hash, 0, true);
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }

            await Task.FromResult(0);
        }
    }
}
