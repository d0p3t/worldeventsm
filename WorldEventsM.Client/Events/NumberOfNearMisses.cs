﻿using System;
using System.Threading.Tasks;
using CitizenFX.Core.UI;
using WorldEventsM.Shared.Enum;

namespace WorldEventsM.Client.Events
{
    public class NumberOfNearMisses : IWorldEvent
    {
        public NumberOfNearMisses(int id, string name, double countdownTime, double seconds) : base(id, name, countdownTime, seconds, false, "AMCH_8", PlayerStats.NumberNearMisses)
        {
            Client.GetInstance().RegisterTickHandler(OnTick);
            Client.GetInstance().RegisterTickHandler(OnStartedEventTick);
        }

        private async Task OnTick()
        {
            try
            {
                if (!IsActive) { return; }

                if (!IsStarted)
                {
                    Screen.ShowSubtitle($"Find a land vehicle and prepare for the {Name} Challenge.", 50);
                }
                else
                {
                    Screen.ShowSubtitle("Perform the most near misses with other vehicles in a land vehicle.", 50);
                }

            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }

            await Task.FromResult(0);
        }

        private async Task OnStartedEventTick()
        {
            try
            {
                if(!IsStarted) { return; }

            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }

            await Task.FromResult(0);
        }
    }
}
