﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CitizenFX.Core.UI;
using static CitizenFX.Core.Native.API;
using WorldEventsM.Shared.Enum;

namespace WorldEventsM.Client.Events
{
    class FastestSpeedInCar : IWorldEvent
    {
        public FastestSpeedInCar(int id, string name, double countdownTime, double seconds) : base(id, name, countdownTime, seconds, true, "Perform the highest speed in a road vehicle", PlayerStats.FastestSpeedInCar, "km/h", PlayerStatType.Float)
        {
            Client.GetInstance().RegisterTickHandler(OnTick);
            Client.GetInstance().RegisterTickHandler(OnStartedEventTick);
        }

        public override void OnEventActivated()
        {
            FirstStartedTick = true;
            base.OnEventActivated();
        }
        private async Task OnTick()
        {
            try
            {
                if (!IsActive) { return; }

                if (!IsStarted)
                {
                    Screen.ShowSubtitle($"Acquire a road vehicle and prepare for the {Name} Challenge.", 50);
                }
                else
                {
                    Screen.ShowSubtitle("Achieve the highest speed in a road vehicle.", 50);
                }

            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }

            await Task.FromResult(0);
        }

        private async Task OnStartedEventTick()
        {
            try
            {
                if (!IsStarted) {return; }

                if (FirstStartedTick)
                {
                    FirstStartedTick = false;
                    var hash = unchecked((uint)PlayerStats.FastestSpeedInCar);
                    StatSetFloat(hash, 0, true);
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(ex);
            }

            await Task.FromResult(0);
        }

        public override void ResetEvent()
        {
            FirstStartedTick = true;
            if (FirstStartedTick)
            {
                FirstStartedTick = false;
                var hash = unchecked((uint)PlayerStats.FastestSpeedInCar);
                StatSetFloat(hash, 0, true);
            }
            base.ResetEvent();
        }
    }
}
