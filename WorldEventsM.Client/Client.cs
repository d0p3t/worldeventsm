﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.UI;
using static CitizenFX.Core.Native.API;
using WorldEventsM.Client.Helpers;

namespace WorldEventsM.Client
{
    public class Client : BaseScript
    {
        private static readonly List<string> Notifications = new List<string>
        {
            "WorldEventsM mimicks GTA:O Freemode Challenges.",
            "Report any bugs on our ~b~Discord~s~ (see /discord)",
            "Twitter:~b~ https://twitter.com/d0p3t ~s~~n~Discord:~f~ https://discord.gg/SBwfEda ~s~",
            "The events are based around so called MP PlayerStats.",
            "See a ~r~bug~s~ or have an idea? Please contact me via ~b~Twitter~s~ or ~b~Discord~s~",
            "The WorldEventsM Gamemode is a contestant for the FiveM Gametype Contest.",
            "Trouble finding an air vehicle? Check out the airports.",
            "As you level up, you unlock vehicles. View them in the interactin menu.",
            "Have an idea for a challenge? Submit it on Discord.",
            "Did you know that there are in total 8 challenges?",
            "Press ~b~M~s~ to access the Interaction Menu"
        };

        private static bool FirstSpawn = true;

        private static Client _instance;

        public static Client GetInstance()
        {
            return _instance;
        }

        public Client()
        {
            _instance = this;
            RegisterTickHandler(DefaultTick);
            RegisterTickHandler(OnNotificationTick);

            RegisterEventHandler("onClientResourceStart", new Action<string>(OnClientResourceStart));
            RegisterEventHandler("playerSpawned", new Action(OnPlayerSpawned));

            RegisterCommand("twitter", new Action<dynamic, List<dynamic>, string>((dynamic source, List<dynamic> args, string rawCommand) =>
            {
                TriggerEvent("chatMessage", "Info", new[] { 255, 0, 0 }, "https://twitter.com/d0p3t");
            }), false);

            RegisterCommand("discord", new Action<dynamic, List<dynamic>, string>((dynamic source, List<dynamic> args, string rawCommand) =>
            {
                TriggerEvent("chatMessage", "Info", new[] { 255, 0, 0 }, "https://discord.gg/SBwfEda");
            }), false);
        }

        private void OnClientResourceStart(string resourceName)
        {
            if(GetCurrentResourceName() != resourceName) { return; }

            SetWeatherTypeNowPersist("EXTRASUNNY");

            SetDiscordAppId("520002684560998461");
            SetDiscordRichPresenceAsset("logo");
            SetDiscordRichPresenceAssetText("Join now!");
        }

        private void OnPlayerSpawned()
        {
            if (FirstSpawn)
            {
                FirstSpawn = false;
                Hud.AdvancedNotification(
                    "Challenge others to earn the most points in events. Rank up to unlock vehicles and other features!",
                    "Welcome!", "WorldEventsM v1.0.1", "CHAR_WE", "REBOOTBOTTOM", HudColor.HUD_COLOUR_FACEBOOK_BLUE,
                    default(System.Drawing.Color), false, NotificationType.Bubble);
                Hud.AdvancedNotification("Press ~r~M~s~ to open the Interaction Menu. Spawn vehicles, enable PvP, and more!", "Interaction Menu", "WorldEventsM v1.0.1", "CHAR_WE",
                    "REBOOTBOTTOM", HudColor.HUD_COLOUR_FACEBOOK_BLUE, default(System.Drawing.Color), false,
                    NotificationType.Bubble);
                TriggerServerEvent("worldEventsManage.Server:GetStatus");
            }
        }

        private async Task OnNotificationTick()
        {
            await Delay(60000 * 5);
            var randomNotification = Notifications.OrderBy(s => Guid.NewGuid()).First();
            Hud.AdvancedNotification(randomNotification, "Information Portal", "", "CHAR_WE", "REBOOTBOTTOM", HudColor.HUD_COLOUR_FACEBOOK_BLUE);
        }

        private async Task DefaultTick()
        {
            ShouldUseMetricMeasurements();
            SetPedDensityMultiplierThisFrame(0.75f);
            SetVehicleDensityMultiplierThisFrame(0.75f);
            SetRandomVehicleDensityMultiplierThisFrame(0.75f);

            await Task.FromResult(0);
        }

        public void RegisterTickHandler(Func<Task> action)
        {
            try
            {
                Tick += action;
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }

        public void DeregisterTickHandler(Func<Task> action)
        {
            try
            {
                Tick -= action;
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }

        public void RegisterEventHandler(string name, Delegate action)
        {
            try
            {
                EventHandlers.Add(name, action);
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }

        public void DeregisterEventHandler(string name)
        {
            try
            {
                EventHandlers.Remove(name);
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }
    }
}
