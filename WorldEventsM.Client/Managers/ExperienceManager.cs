﻿using System;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.UI;
using WorldEventsM.Client.Helpers;
using WorldEventsM.Shared.Helpers;

namespace WorldEventsM.Client.Managers
{
    public class ExperienceManager : BaseScript
    {
        public ExperienceManager()
        {
            Client.GetInstance().RegisterEventHandler("worldEventsManage.Client.UpdateExperience", new Action<int, int, int, int, int, int, int, int, bool>(OnUpdateExperience));
        }

        private async void OnUpdateExperience(int currentRankLimit, int nextRankLimit, int updatedCurrentRankLimit, int updatedNextRankLimit, int currentXp, int updatedXp, int currentLevel, int updatedLevel, bool leveledUp)
        {
            try
            {
                Constants.WorldEventPlayer.Level = updatedLevel;
                Constants.WorldEventPlayer.TotalXp = updatedXp;

                if (!leveledUp)
                {
                    await Hud.ShowPlayerRankScoreAfterUpdate(currentRankLimit, nextRankLimit, currentXp, updatedXp, currentLevel);
                }
                else
                {
                    TriggerEvent("worldeventsManage.Client:UpdatedLevel", updatedLevel, true);
                    var newVehiclesCount = 0;
                    foreach (var vehicle in Unlocks.Vehicles)
                    {
                        if (vehicle.Value == updatedLevel)
                        {
                            newVehiclesCount++;
                        }
                    }

                    if (newVehiclesCount != 0)
                    {
                        Hud.AdvancedNotification($"You have unlocked {newVehiclesCount} new Vehicles", "New Unlocks", "", "CHAR_WE", "REBOOTBOTTOM", HudColor.HUD_COLOUR_MENU_YELLOW, default(System.Drawing.Color), false, NotificationType.Mail);
                    }

                    await Hud.ShowPlayerRankScoreAfterUpdate(currentRankLimit, nextRankLimit, currentXp, nextRankLimit, currentLevel);
                    await Delay(2000);
                    await Hud.ShowPlayerRankScoreAfterUpdate(updatedCurrentRankLimit, updatedNextRankLimit, 0, updatedXp - currentXp, updatedLevel);
                }
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }

            await Task.FromResult(0);
        }
    }
}
