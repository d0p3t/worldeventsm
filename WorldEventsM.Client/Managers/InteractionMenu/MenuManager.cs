﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using CitizenFX.Core;
using MenuAPI;
using Newtonsoft.Json;
using WorldEventsM.Client.Helpers;
using WorldEventsM.Shared.Helpers;
using WorldEventsM.Shared.Model;
using static CitizenFX.Core.Native.API;

namespace WorldEventsM.Client.Managers.InteractionMenu
{
    public class MenuManager : BaseScript
    {
        private static readonly Dictionary<string, Vector3> quickGPSItems = new Dictionary<string, Vector3>
        {
            {"Benny's Motor Works", new Vector3(-368.6f, -139.85f, 38.7f)},
            {"Grapeseed", new Vector3(2186f, 4748.5f, 41.2f)},
            {"Legion Square", new Vector3(195f, -933.5f, 30.7f)},
            {"Los Santos Customs", new Vector3(715f, -1085.5f, 22.4f)},
            {"LS International Airport", new Vector3(-1127.072f, -2690.774f, 13.959f)},
            {"Sandy Shores", new Vector3(1695.5f, 3506.5f, 36.5f)}
        };

        private Vehicle lastSpawnedVehicle;
        private PlayerSettings playerSettings;

        private Menu mainMenu;
        private Menu spawnVehicleMenu;
        private Menu appearanceMenu;
        private Menu customizeVehicleMenu;
        private Menu playerSettingsMenu;

        // Submenus
        private Menu spawnVehicleCarMenu;
        private Menu spawnVehicleHeliMenu;
        private Menu spawnVehicleMotorcycleMenu;
        private Menu spawnVehicleBicycleMenu;
        private Menu spawnVehiclePlaneMenu;
        private Menu spawnVehicleUnknownMenu;
        private MenuItem spawnVehicleButton;
        private MenuItem spawnVehicleCarButton;
        private MenuItem spawnVehicleHeliButton;
        private MenuItem spawnVehicleMotorcycleButton;
        private MenuItem spawnVehicleBicycleButton;
        private MenuItem spawnVehiclePlaneButton;
        private MenuItem spawnVehicleUnknownButton;

        public MenuManager()
        {
            Client.GetInstance().RegisterTickHandler(OnTick);
            Client.GetInstance().RegisterEventHandler("worldeventsManage.Client:UpdatedLevel", new Action<int, bool>(OnUpdatedLevel));
            Client.GetInstance().RegisterEventHandler("onClientResourceStart", new Action<string>(OnClientResourceStart));
        }

        private async Task OnTick()
        {
            try
            {
                if (Game.IsControlJustPressed(0, Control.InteractionMenu)) mainMenu.Visible = true;
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }

            await Task.FromResult(0);
        }

        private void OnClientResourceStart(string resourceName)
        {
            if (GetCurrentResourceName() != resourceName) return;

            Initialise();
        }

        private void OnUpdatedLevel(int myLevel, bool levelUp)
        {
            try
            {
                spawnVehicleCarMenu.ClearMenuItems();
                spawnVehicleHeliMenu.ClearMenuItems();
                spawnVehicleBicycleMenu.ClearMenuItems();
                spawnVehicleMotorcycleMenu.ClearMenuItems();
                spawnVehiclePlaneMenu.ClearMenuItems();
                spawnVehicleUnknownMenu.ClearMenuItems();

                foreach (var vehicle in Unlocks.Vehicles)
                {
                    var vehicleMenuItem = new MenuItem(vehicle.Key.ToTitleCase());
                    
                    if (vehicle.Value <= myLevel)
                    {
                        if (levelUp)
                        {
                            vehicleMenuItem.RightIcon = MenuItem.Icon.STAR;

                            spawnVehicleButton.RightIcon = MenuItem.Icon.STAR;
                        }

                        vehicleMenuItem.Enabled = true;
                    }
                    else
                    {
                        vehicleMenuItem.Enabled = false;
                        vehicleMenuItem.Description = $"Requires Level ~r~{vehicle.Value}~s~ to Unlock";
                        vehicleMenuItem.RightIcon = MenuItem.Icon.LOCK;
                    }

                    var hashKey = (uint) GetHashKey(vehicle.Key);
                    if (IsThisModelACar(hashKey))
                        spawnVehicleCarMenu.AddMenuItem(vehicleMenuItem);
                    else if (IsThisModelABicycle(hashKey))
                        spawnVehicleBicycleMenu.AddMenuItem(vehicleMenuItem);
                    else if (IsThisModelABike(hashKey))
                        spawnVehicleMotorcycleMenu.AddMenuItem(vehicleMenuItem);
                    else if (IsThisModelAHeli(hashKey))
                        spawnVehicleHeliMenu.AddMenuItem(vehicleMenuItem);
                    else if (IsThisModelAPlane(hashKey))
                        spawnVehiclePlaneMenu.AddMenuItem(vehicleMenuItem);
                    else
                        spawnVehicleUnknownMenu.AddMenuItem(vehicleMenuItem);
                }
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }

        private void Initialise()
        {
            var kvpSettings = GetResourceKvpString("weplayersettings");

            if (string.IsNullOrEmpty(kvpSettings))
                playerSettings = new PlayerSettings();
            else
                playerSettings = JsonConvert.DeserializeObject<PlayerSettings>(kvpSettings);

            UpdatePvp(playerSettings.PvPEnabled);
            NetworkSetTalkerProximity(playerSettings.VoiceDistance);

            var playerName = Game.Player.Name;

            MenuController.MenuAlignment = MenuController.MenuAlignmentOption.Right;
            mainMenu = new Menu(playerName, "Interaction Menu") {Visible = false};
            mainMenu.EnableInstructionalButtons = false;
            MenuController.AddMenu(mainMenu);

            var list = new List<string>();
            foreach (var item in quickGPSItems.Keys)
            {
                list.Add(item);
            }

            var quickGPSMenuItem = new MenuListItem("Quick GPS", list, 0, "Quickly Navigate to a common location");
            mainMenu.AddMenuItem(quickGPSMenuItem);
            #region Submenus

            spawnVehicleMenu = new Menu(playerName, "Spawn Vehicle");
            spawnVehicleMenu.EnableInstructionalButtons = false;
            MenuController.AddSubmenu(mainMenu, spawnVehicleMenu);

            spawnVehicleCarMenu = new Menu(playerName, "Cars");
            spawnVehicleCarMenu.EnableInstructionalButtons = false;
            MenuController.AddSubmenu(spawnVehicleMenu, spawnVehicleCarMenu);

            spawnVehicleMotorcycleMenu = new Menu(playerName, "Motorcycles");
            spawnVehicleMotorcycleMenu.EnableInstructionalButtons = false;
            MenuController.AddSubmenu(spawnVehicleMenu, spawnVehicleMotorcycleMenu);

            spawnVehicleBicycleMenu = new Menu(playerName, "Bicycles");
            spawnVehicleBicycleMenu.EnableInstructionalButtons = false;
            MenuController.AddSubmenu(spawnVehicleMenu, spawnVehicleBicycleMenu);

            spawnVehicleHeliMenu = new Menu(playerName, "Helicopters");
            spawnVehicleHeliMenu.EnableInstructionalButtons = false;
            MenuController.AddSubmenu(spawnVehicleMenu, spawnVehicleHeliMenu);

            spawnVehiclePlaneMenu = new Menu(playerName, "Planes");
            spawnVehiclePlaneMenu.EnableInstructionalButtons = false;
            MenuController.AddSubmenu(spawnVehicleMenu, spawnVehiclePlaneMenu);

            spawnVehicleUnknownMenu = new Menu(playerName, "Others");
            spawnVehicleUnknownMenu.EnableInstructionalButtons = false;
            MenuController.AddSubmenu(spawnVehicleMenu, spawnVehicleUnknownMenu);

            spawnVehicleButton = new MenuItem("Spawn Vehicle", "Spawn Unlocked Vehicles")
            {
                Label = "→→→"
            };

            mainMenu.AddMenuItem(spawnVehicleButton);
            MenuController.BindMenuItem(mainMenu, spawnVehicleMenu, spawnVehicleButton);

            spawnVehicleCarButton = new MenuItem("Cars")
            {
                Label = "→→→"
            };

            spawnVehicleMenu.AddMenuItem(spawnVehicleCarButton);
            MenuController.BindMenuItem(spawnVehicleMenu, spawnVehicleCarMenu, spawnVehicleCarButton);

            spawnVehicleMotorcycleButton = new MenuItem("Motorcycles")
            {
                Label = "→→→"
            };

            spawnVehicleMenu.AddMenuItem(spawnVehicleMotorcycleButton);
            MenuController.BindMenuItem(spawnVehicleMenu, spawnVehicleMotorcycleMenu, spawnVehicleMotorcycleButton);

            spawnVehicleBicycleButton = new MenuItem("Bicycles")
            {
                Label = "→→→"
            };

            spawnVehicleMenu.AddMenuItem(spawnVehicleBicycleButton);
            MenuController.BindMenuItem(spawnVehicleMenu, spawnVehicleBicycleMenu, spawnVehicleBicycleButton);

            spawnVehicleHeliButton = new MenuItem("Helicopters")
            {
                Label = "→→→"
            };

            spawnVehicleMenu.AddMenuItem(spawnVehicleHeliButton);
            MenuController.BindMenuItem(spawnVehicleMenu, spawnVehicleHeliMenu, spawnVehicleHeliButton);

            spawnVehiclePlaneButton = new MenuItem("Planes")
            {
                Label = "→→→"
            };

            spawnVehicleMenu.AddMenuItem(spawnVehiclePlaneButton);
            MenuController.BindMenuItem(spawnVehicleMenu, spawnVehiclePlaneMenu, spawnVehiclePlaneButton);

            spawnVehicleUnknownButton = new MenuItem("Others")
            {
                Label = "→→→"
            };

            spawnVehicleMenu.AddMenuItem(spawnVehicleUnknownButton);
            MenuController.BindMenuItem(spawnVehicleMenu, spawnVehicleUnknownMenu, spawnVehicleUnknownButton);

            customizeVehicleMenu = new Menu("Customize Vehicle", "Coming Soon - Customize Your Vehicle");
            customizeVehicleMenu.EnableInstructionalButtons = false;
            MenuController.AddSubmenu(mainMenu, customizeVehicleMenu);

            var customizeVehicleButton = new MenuItem("Customize Vehicle", "Coming Soon - Customize Your Vehicle")
            {
                Label = "→→→",
                Enabled = false
            };

            mainMenu.AddMenuItem(customizeVehicleButton);
            MenuController.BindMenuItem(mainMenu, customizeVehicleMenu, customizeVehicleButton);

            appearanceMenu = new Menu("Appearance", "Coming Soon - Customize Your Look");
            appearanceMenu.EnableInstructionalButtons = false;
            MenuController.AddSubmenu(mainMenu, appearanceMenu);

            var appearanceMenuButton = new MenuItem("Appearance", "Coming Soon - Customize Your Look")
            {
                Label = "→→→",
                Enabled = false
            };

            mainMenu.AddMenuItem(appearanceMenuButton);
            MenuController.BindMenuItem(mainMenu, appearanceMenu, appearanceMenuButton);

            var genderSlider = new MenuSliderItem("Gender", "Choose your gender", 0, 1, 0, false)
            {
                BarColor = Color.FromArgb(255, 255, 0, 0),
                BackgroundColor = Color.FromArgb(255, 100, 0, 0),
                SliderLeftIcon = MenuItem.Icon.MALE,
                SliderRightIcon = MenuItem.Icon.FEMALE
            };

            appearanceMenu.AddMenuItem(genderSlider);

            playerSettingsMenu = new Menu(playerName, "Player Settings");
            playerSettingsMenu.EnableInstructionalButtons = false;
            MenuController.AddSubmenu(mainMenu, playerSettingsMenu);
            var playerSettingsMenuButton = new MenuItem("Settings", "Change your player settings (Voice, Indicators, PvP etc.)")
            {
                Label = "→→→"
            };

            mainMenu.AddMenuItem(playerSettingsMenuButton);
            MenuController.BindMenuItem(mainMenu, playerSettingsMenu, playerSettingsMenuButton);

            var pVpCheckbox = new MenuCheckboxItem("~r~PvP Enabled", "Friendly Fire", true)
            {
                Style = MenuCheckboxItem.CheckboxStyle.Tick,
                Checked = playerSettings.PvPEnabled,
                Text = $"{(!playerSettings.PvPEnabled ? "~r~PvP Disabled" : "~g~PvP Enabled")}"
            };

            playerSettingsMenu.AddMenuItem(pVpCheckbox);

            var voiceProximity = new MenuSliderItem($"Voice Proximity [{playerSettings.VoiceDistance}m]", "Change your voice listen proximity", 0, 40, (int)playerSettings.VoiceDistance, false);
            playerSettingsMenu.AddMenuItem(voiceProximity);

            var exitButton = new MenuItem("Save & Exit", "Saves your settings and exits menu.");
            mainMenu.AddMenuItem(exitButton);

            #endregion

            #region EventHandlers

            mainMenu.OnListItemSelect += (_menu, _listItem, _selectedIndex, _itemIndex) =>
            {
                if (_listItem == quickGPSMenuItem)
                {
                    var locationName = quickGPSMenuItem.ListItems[_selectedIndex];
                    World.WaypointPosition = quickGPSItems[locationName];
                }
            };

            appearanceMenu.OnSliderItemSelect += async (_menu, _sliderItem, _sliderPosition, _itemIndex) =>
            {
                if (_sliderItem == genderSlider)
                {
                    if (_sliderPosition == 0)
                    {
                        if (Game.PlayerPed.Model != PedHash.FreemodeMale01)
                        {
                            await Game.Player.ChangeModel(PedHash.FreemodeMale01);
                            Game.PlayerPed.Style.SetDefaultClothes();
                        }
                    }
                    else
                    {
                        if (Game.PlayerPed.Model != PedHash.FreemodeFemale01)
                        {
                            await Game.Player.ChangeModel(PedHash.FreemodeFemale01);
                            Game.PlayerPed.Style.SetDefaultClothes();
                        }
                    }
                }
            };

            playerSettingsMenu.OnSliderPositionChange += async (_menu, _sliderItem, _sliderOldPosition, _sliderNewPosition, _itemIndex) =>
            {
                if (_sliderItem == voiceProximity)
                {
                    NetworkSetTalkerProximity(_sliderNewPosition);
                    _sliderItem.Text = $"Voice Proximity [{_sliderNewPosition}m]";
                }
            };

            spawnVehicleCarMenu.OnItemSelect += async (_menu, _item, _index) => { await SpawnMenuVehicle(_item.Text); };

            spawnVehicleMotorcycleMenu.OnItemSelect += async (_menu, _item, _index) =>
            {
                await SpawnMenuVehicle(_item.Text);
            };

            spawnVehicleBicycleMenu.OnItemSelect += async (_menu, _item, _index) =>
            {
                await SpawnMenuVehicle(_item.Text);
            };

            spawnVehicleHeliMenu.OnItemSelect += async (_menu, _item, _index) =>
            {
                await SpawnMenuVehicle(_item.Text);
            };

            spawnVehiclePlaneMenu.OnItemSelect += async (_menu, _item, _index) =>
            {
                await SpawnMenuVehicle(_item.Text);
            };

            spawnVehicleCarMenu.OnIndexChange += (_menu, _oldItem, _newItem, _oldIndex, _newIndex) =>
            {
                UpdateIcon(_menu, spawnVehicleCarButton, _oldItem);
            };

            spawnVehicleMotorcycleMenu.OnIndexChange += (_menu, _oldItem, _newItem, _oldIndex, _newIndex) =>
            {
                UpdateIcon(_menu, spawnVehicleMotorcycleButton, _oldItem);
            };

            spawnVehicleBicycleMenu.OnIndexChange += (_menu, _oldItem, _newItem, _oldIndex, _newIndex) =>
            {
                UpdateIcon(_menu, spawnVehicleBicycleButton, _oldItem);
            };

            spawnVehicleHeliMenu.OnIndexChange += (_menu, _oldItem, _newItem, _oldIndex, _newIndex) =>
            {
                UpdateIcon(_menu, spawnVehicleHeliButton, _oldItem);
            };

            spawnVehiclePlaneMenu.OnIndexChange += (_menu, _oldItem, _newItem, _oldIndex, _newIndex) =>
            {
                UpdateIcon(_menu, spawnVehiclePlaneButton, _oldItem);
            };

            playerSettingsMenu.OnCheckboxChange += (_menu, _item, _index, _checked) =>
            {
                if (_item == pVpCheckbox)
                {
                    if (_checked)
                    {
                        UpdatePvp(true);
                    }
                    else
                    {
                        UpdatePvp(false);
                    }

                    pVpCheckbox.Text = $"{(!_checked ? "~r~PvP Disabled" : "~g~PvP Enabled")}";
                }
            };

            mainMenu.OnItemSelect += (_menu, _item, _index) =>
            {
                if (_item == exitButton) mainMenu.CloseMenu();
            };

            mainMenu.OnMenuClose += _menu =>
            {
                var json = JsonConvert.SerializeObject(playerSettings);
                SetResourceKvp("weplayersettings", json);

                Logger.Debug("Closed Menu");
            };

            #endregion
        }

        private void SpawnVehicleMenu_OnItemSelect(Menu menu, MenuItem menuItem, int itemIndex)
        {
            throw new NotImplementedException();
        }

        private async Task SpawnMenuVehicle(string model)
        {
            if (Game.PlayerPed.HeightAboveGround > 3f)
            {
                Hud.AdvancedNotification("You must be on the ground to spawn a vehicle.", "Oops!", "Spawning Vehicle Failed", bgColor: HudColor.HUD_COLOUR_REDDARK);
                return;
            }

            var currentVehicle = Game.PlayerPed.CurrentVehicle;

            if (currentVehicle != null) currentVehicle.Delete();

            if (lastSpawnedVehicle != null && lastSpawnedVehicle.Exists()) lastSpawnedVehicle.Delete();

            var vehicle = await World.CreateVehicle(new Model(model), Game.PlayerPed.Position, Game.PlayerPed.Heading);

            vehicle.NeedsToBeHotwired = false;
            vehicle.IsEngineStarting = true;
            lastSpawnedVehicle = vehicle;

            Game.PlayerPed.SetIntoVehicle(vehicle, VehicleSeat.Driver);
        }

        private void UpdateIcon(Menu menu, MenuItem button, MenuItem oldItem)
        {
            if (oldItem.RightIcon == MenuItem.Icon.STAR) oldItem.RightIcon = MenuItem.Icon.NONE;

            if (menu.GetMenuItems().Find(x => x.RightIcon == MenuItem.Icon.STAR) == null)
                button.RightIcon = MenuItem.Icon.NONE;
        }

        private void UpdatePvp(bool toggle)
        {
            playerSettings.PvPEnabled = toggle;
            NetworkSetFriendlyFireOption(toggle);
            SetCanAttackFriendly(PlayerPedId(), toggle, false);
        }
    }
}