﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.UI;
using static CitizenFX.Core.Native.API;
using Newtonsoft.Json;
using NativeUI;
using WorldEventsM.Shared.Helpers;
using WorldEventsM.Client.Events;
using WorldEventsM.Client.Helpers;

namespace WorldEventsM.Client.Managers
{
    public class WorldEventsManager : BaseScript
    {
        private static List<IWorldEvent> WorldEvents = new List<IWorldEvent>();

        private static TimerBarPool _timerBarPool = new TimerBarPool();

        public static IWorldEvent ActiveWorldEvent;
        public static IWorldEvent NextWorldEvent;

        private static int JoinWaitTime = 0;
        private static int DownTime = 0;
        public static bool WarningMessageDisplayed = false;
        private static bool FirstSpawn = true;
        private static readonly Random rnd = new Random();

        public WorldEventsManager()
        {
            Client.GetInstance().RegisterTickHandler(OnDefaultTick);
            Client.GetInstance().RegisterTickHandler(OnDiscordTick);
            Client.GetInstance().RegisterTickHandler(OnWaitTick);

            Client.GetInstance().RegisterEventHandler("worldEventsManage.Client:EventActivate",new Action<int, int>(OnActivateEvent));
            Client.GetInstance().RegisterEventHandler("worldeventsManager.Client:GetEventData",new Action<int, float, float>(OnGetEventData));
            Client.GetInstance().RegisterEventHandler("worldEventsManage.Client:Status", new Action<int, int, int, int, bool>(OnGetStatus));
            Client.GetInstance().RegisterEventHandler("worldEventsManage.Client:NextEventIn", new Action<int>(OnNextEventIn));
            Client.GetInstance().RegisterEventHandler("worldEventsManage.Client:GetTop3", new Action<string>(OnGetTop3));
            Client.GetInstance().RegisterEventHandler("worldEventsManage.Client:FinalTop3", new Action<int, string>(OnGetFinalTop3));
            Client.GetInstance().RegisterEventHandler("worldEventsManage.Client:PeriodicSync", new Action<int, bool>(OnPeriodicSync));
            Client.GetInstance().RegisterEventHandler("onClientResourceStart", new Action<string>(OnClientResourceStart));
        }

        private async Task OnWaitTick()
        {
            try
            {
                await BaseScript.Delay(1000);

                if(JoinWaitTime != 0)
                    JoinWaitTime = JoinWaitTime - 1;

                if (DownTime != 0)
                    DownTime = DownTime - 1;
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }

            await Task.FromResult(0);
        }

        private async Task OnDiscordTick()
        {
            try
            {
                await BaseScript.Delay(1000);

                if (!ActiveWorldEvent.IsActive)
                {
                    SetRichPresence("Preparing Next Event");
                    SetDiscordRichPresenceAssetSmallText("Loading...");
                    SetDiscordRichPresenceAssetSmall("we-prepare");
                    return;
                }

                SetDiscordRichPresenceAssetSmall($"we-num{ActiveWorldEvent.Id}");
                SetDiscordRichPresenceAssetSmallText($"{(ActiveWorldEvent.IsStarted ? ActiveWorldEvent.TimeRemaining.ToString(@"mm\:ss") : ActiveWorldEvent.CountdownTime.ToString(@"mm\:ss"))} {ActiveWorldEvent.Name}");

                SetRichPresence($"{(ActiveWorldEvent.IsStarted ? $"Current Attempt {Math.Round(ActiveWorldEvent.CurrentAttempt, 0)}{ActiveWorldEvent.StatUnit}" : $"Starting in {ActiveWorldEvent.CountdownTime.ToString(@"mm\:ss")}")}");
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }

            await Task.FromResult(0);
        } 

        private async Task OnDefaultTick()
        {
            try
            {   
                Screen.DisplayHelpTextThisFrame($"~y~Online Players: {new PlayerList().Count()}/32 ~s~~n~~o~Level: {Constants.WorldEventPlayer.Level} XP: {Constants.WorldEventPlayer.TotalXp}/{Experience.RankRequirement[Constants.WorldEventPlayer.Level + 1]} ~s~~n~Now: ~g~{(ActiveWorldEvent != null ? ActiveWorldEvent.Name : "None")} ~n~~s~Next: ~b~{(NextWorldEvent != null ? NextWorldEvent.Name : "None")}");
                if (Hud.WarningDisplayed)
                {
                    Hud.ShowWarningMessage();
                }
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }

            await Task.FromResult(0);
        }

        private void OnClientResourceStart(string resourceName)
        {
            if (GetCurrentResourceName() != resourceName) { return; }

            TriggerServerEvent("worldEventsManage.Server:AddParticipant");
            WorldEvents.Add(new NumberOfNearMisses(1, "Number of Near Misses", 60, 300));
            WorldEvents.Add(new FlyingUnderBridges(2, "Flying Under Bridges", 90, 300));
            WorldEvents.Add(new FastestSpeedInCar(3, "Fastest Speed", 60, 270));
            WorldEvents.Add(new MostPistolHeadshots(4, "Most Pistol Headshots", 60, 300));
            WorldEvents.Add(new LongestSurvivedFreefall(5, "Longest Survived Freefall", 60, 240));
            WorldEvents.Add(new HighestSkittles(6, "Highest Skittles", 60, 300));
            WorldEvents.Add(new FarthestJumpDistance(7, "Longest Jump", 60, 270));
            WorldEvents.Add(new HighestJumpDistance(8, "Highest Jump", 60, 270));
            TriggerServerEvent("worldEventsManage.Server:GetStatus");
        }

        private void OnPeriodicSync(int eventTime, bool isStarted)
        {
            if(ActiveWorldEvent == null) { return; }

            if (isStarted)
            {
                if(ActiveWorldEvent.TimeRemaining.TotalSeconds != eventTime)
                    ActiveWorldEvent.TimeRemaining = TimeSpan.FromSeconds(eventTime);
            }
            else
            {
                if(ActiveWorldEvent.CountdownTime.TotalSeconds != eventTime)
                    ActiveWorldEvent.CountdownTime = TimeSpan.FromSeconds(eventTime);
            }
        }

        private void OnGetEventData(int eventId, float currentAttempt, float bestAttempt)
        {
            if (ActiveWorldEvent == null){ return; }

            ActiveWorldEvent.CurrentAttempt = currentAttempt;
            ActiveWorldEvent.BestAttempt = bestAttempt;
        }

        private async void OnGetFinalTop3(int eventId, string json)
        {
            try
            {
                var top3 = JsonConvert.DeserializeObject<Dictionary<string, float>>(json);

                var xp = 0;
                var place = 0;

                // Winner
                if (top3.ElementAt(0).Key == Game.Player.Name)
                {
                    if (top3.ElementAt(0).Value != 0)
                    {
                        xp = Convert.ToInt32(top3.ElementAt(0).Value * 1.75);
                        place = 1;
                    }
                }
                // 2nd Place
                else if (top3.ElementAt(1).Key == Game.Player.Name)
                {
                    if (top3.ElementAt(1).Value != 0)
                    {
                        xp = Convert.ToInt32(top3.ElementAt(1).Value * 1.5);
                        place = 2;
                    }
                }
                // 3rd Place
                else if (top3.ElementAt(2).Key == Game.Player.Name)
                {
                    if (top3.ElementAt(2).Value != 0)
                    {
                        xp = Convert.ToInt32(top3.ElementAt(2).Value * 1.25);
                        place = 3;
                    }
                }
                // Out of top 3
                else
                {
                    place = 0;
                }

                var title = "Lost";
                var score = "";
                if (ActiveWorldEvent.PlayerStatType == Shared.Enum.PlayerStatType.Int)
                {
                    score = Math.Round(ActiveWorldEvent.CurrentAttempt, 0).ToString();
                }
                else if (ActiveWorldEvent.PlayerStatType == Shared.Enum.PlayerStatType.Float)
                {
                    score = Math.Round(ActiveWorldEvent.CurrentAttempt, 2).ToString();
                }

                var description = $"You didn't place in the top 3 of the {ActiveWorldEvent.Name} Challenge with a score of {score}{ActiveWorldEvent.StatUnit}";
                switch (place)
                {
                    case 1:
                        title = "Winner";
                        description = $"You are the Winner of the {ActiveWorldEvent.Name} Challenge with a score of {score}{ActiveWorldEvent.StatUnit}!";
                        Audio.PlaySoundFrontend("FIRST_PLACE", "HUD_MINI_GAME_SOUNDSET");
                        break;
                    case 2:
                        title = "Second";
                        description = $"So close, yet so far away... You placed 2nd in the {ActiveWorldEvent.Name} Challenge with a score of {score}{ActiveWorldEvent.StatUnit}";
                        Audio.PlaySoundFrontend("RACE_PLACED", "HUD_AWARDS");
                        break;
                    case 3:
                        title = "Third";
                        description = $"You placed 3rd in the {ActiveWorldEvent.Name} Challenge with a score of {score}{ActiveWorldEvent.StatUnit}";
                        Audio.PlaySoundFrontend("RACE_PLACED", "HUD_AWARDS");
                        break;
                    default:
                        Audio.PlaySoundFrontend("RACE_PLACED", "HUD_AWARDS");
                        break;
                }

                MediumMessageBase.MessageInstance.ShowColoredShard(title, description, Helpers.HudColor.HUD_COLOUR_PURPLE, true, false, 7500);
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }

            await Task.FromResult(0);
        }

        private void OnGetTop3(string top3)
        {
            try
            {
                if (ActiveWorldEvent == null)
                {
                    return;
                }

                var top3Players = JsonConvert.DeserializeObject<Dictionary<string, float>>(top3);

                if (top3Players.Count < 3) { return; }

                ActiveWorldEvent.FirstPlaceTimerBar.Label = $"~y~1st: {top3Players.ElementAt(0).Key}";
                ActiveWorldEvent.FirstPlaceTimerBar.Text = $"~y~{Math.Round(top3Players.ElementAt(0).Value, 2)} {ActiveWorldEvent.StatUnit}";
                ActiveWorldEvent.SecondPlaceTimerBar.Label = $"~c~2nd: {top3Players.ElementAt(1).Key}";
                ActiveWorldEvent.SecondPlaceTimerBar.Text = $"~c~{Math.Round(top3Players.ElementAt(1).Value, 2)} {ActiveWorldEvent.StatUnit}";
                ActiveWorldEvent.ThirdPlaceTimerBar.Label = $"~o~3rd: {top3Players.ElementAt(2).Key}";
                ActiveWorldEvent.ThirdPlaceTimerBar.Text = $"~o~{Math.Round(top3Players.ElementAt(2).Value, 2)} {ActiveWorldEvent.StatUnit}";
            }
            catch (Exception e)
            {
                Logger.Exception(e);
            }
        }

        private void OnActivateEvent(int currentId, int nextId)
        {
            JoinWaitTime = 0;
            ActiveWorldEvent = WorldEvents.Where(x => x.Id == currentId).FirstOrDefault();
            NextWorldEvent = WorldEvents.Where(x => x.Id == nextId).FirstOrDefault();

            if (ActiveWorldEvent != null)
            {
                ActiveWorldEvent.Activate(true);
            }
        }

        private void OnNextEventIn(int seconds)
        {
            DownTime = seconds;

            if (DownTime > 0) { Screen.LoadingPrompt.Show("Preparing Next Event"); }
        }

        private void OnGetStatus(int currentId, int nextId, int downTime, int joinWaitTime, bool isStarted)
        {
            ActiveWorldEvent = WorldEvents.Where(x => x.Id == currentId).FirstOrDefault();
            NextWorldEvent = WorldEvents.Where(x => x.Id == nextId).FirstOrDefault();

            DownTime = downTime;
            JoinWaitTime = joinWaitTime;

            if (DownTime > 0)
            {
                Screen.LoadingPrompt.Show("Preparing Next Event");
                return;
            }

            if (FirstSpawn)
            {
                ActiveWorldEvent.ResetEvent();

                if (JoinWaitTime > 0)
                {
                    ActiveWorldEvent.Activate(true);
                    ActiveWorldEvent.IsStarted = isStarted;

                    if (ActiveWorldEvent.IsStarted)
                    {
                        ActiveWorldEvent.ActivateEventTimerBars(false);
                        ActiveWorldEvent.TimeRemaining = TimeSpan.FromSeconds(JoinWaitTime);
                        ActiveWorldEvent.CountdownStarted = false;
                    }
                    else
                    {
                        ActiveWorldEvent.ActivateEventTimerBars(true);
                        ActiveWorldEvent.CountdownTime = TimeSpan.FromSeconds(JoinWaitTime);
                        ActiveWorldEvent.CountdownStarted = true;
                    }
                }
                FirstSpawn = false;
            }
            else
            {
                ActiveWorldEvent.IsStarted = isStarted;

                if (ActiveWorldEvent.IsStarted)
                {
                    ActiveWorldEvent.TimeRemaining = TimeSpan.FromSeconds(JoinWaitTime);
                }
                else
                {
                    ActiveWorldEvent.CountdownTime = TimeSpan.FromSeconds(JoinWaitTime);
                }
            }
        }
    }
}
