﻿using System;

namespace WorldEventsM.Client
{
    public static class Logger
    {
        public static void Debug(string msg)
        {
            CitizenFX.Core.Debug.WriteLine($"[{DateTime.UtcNow}][DEBUG] {msg}");
        }
        public static void Info(string msg)
        {
            CitizenFX.Core.Debug.WriteLine($"[{DateTime.UtcNow}][INFO] {msg}");
        }

        public static void Warning(string msg)
        {
            CitizenFX.Core.Debug.WriteLine($"[WARNING] {msg}");
        }

        public static void Exception(Exception ex)
        {
            CitizenFX.Core.Debug.WriteLine($"[{DateTime.UtcNow}][FATAL] Exception: {ex.Message}");
        }
    }
}
